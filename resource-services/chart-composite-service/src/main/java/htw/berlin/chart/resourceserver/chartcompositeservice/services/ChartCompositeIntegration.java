package htw.berlin.chart.resourceserver.chartcompositeservice.services;


import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.net.URI;
import java.util.List;

import htw.berlin.recourceserver.util.http.HttpErrorInfo;
import htw.berlin.recourceserver.util.http.ServiceUtil;
import htw.berlin.resourceserver.api.core.chart.Chart;
import htw.berlin.resourceserver.api.core.chart.IChartService;
import htw.berlin.resourceserver.api.core.data.Data;
import htw.berlin.resourceserver.api.core.data.IDataService;
import htw.berlin.resourceserver.api.event.Event;
import htw.berlin.resourceserver.api.exceptions.InvalidInputException;
import htw.berlin.resourceserver.api.exceptions.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;

import static htw.berlin.resourceserver.api.event.Event.Type.*;
import static java.util.logging.Level.FINE;
import static reactor.core.publisher.Mono.empty;

@Component
public class ChartCompositeIntegration implements IChartService, IDataService {

    private static final Logger LOG = LoggerFactory.getLogger(ChartCompositeIntegration.class);

    private static final String CHART_SERVICE_URL = "http://chart:7001/chart";
    private static final String CHART_CHART_SERVICE_URL = "http://chart:7001/chart-chart";
    private static final String DATA_SERVICE_URL = "http://data:7002/data";

    private static final String CHART_DATA_SERVICE_URL = "http://data:7002/chart-data";
    private static final String DATA_ALL = "http://data:7002/data-all";
    private static final String GET_SEMESTER = "http://data:7002/getSemesters";

    private final Scheduler publishEventScheduler;
    private final WebClient webClient;
    private final ObjectMapper mapper;
    private final StreamBridge streamBridge;

    private final ServiceUtil serviceUtil;

    @Autowired
    public ChartCompositeIntegration(@Qualifier("publishEventScheduler") Scheduler publishEventScheduler, WebClient.Builder webClientBuilder, ObjectMapper mapper, StreamBridge streamBridge, ServiceUtil serviceUtil) {
        this.publishEventScheduler = publishEventScheduler;
        this.webClient = webClientBuilder.build();
        this.mapper = mapper;
        this.streamBridge = streamBridge;
        this.serviceUtil = serviceUtil;
    }

    @Override
    public Mono<Chart> createChart(Chart body) {
        return Mono.fromCallable(()->{
            sendMessage("charts-out-0", new Event(CREATE, body.getChartId(), body));

            return body;
        }).subscribeOn(publishEventScheduler);
    }



    @Override
    public Flux<Chart> getCharts(String  studentId) {
        URI url = UriComponentsBuilder.fromUriString(CHART_SERVICE_URL + "?studentId={studentId}").build(studentId);
        LOG.debug("Will call the getChart API on URL: {}", url);

        return webClient.get().uri(url)
                .retrieve().bodyToFlux(Chart.class).log(LOG.getName(), FINE)
                .onErrorMap(WebClientResponseException.class, ex -> handleException(ex));
    }

    @Override
    public Mono<Chart> getChartChart(String chartId) {
        URI url = UriComponentsBuilder.fromUriString(CHART_CHART_SERVICE_URL + "?chartId={chartId}").build(chartId);
        LOG.debug("Will call the getChartChart API on URL: {}", url);

        return webClient.get().uri(url)
                .retrieve().bodyToMono(Chart.class).log(LOG.getName(), FINE)
                .onErrorMap(WebClientResponseException.class, ex -> handleException(ex));
    }


    @Override
    public Mono<Void> deleteChart(String chartId) {
        return Mono.fromRunnable(() -> sendMessage("charts-out-0", new Event(DELETE, chartId, null)))
                .subscribeOn(publishEventScheduler).then();
    }

    @Override
    public Mono<Data> createData(Data body) {
        return Mono.fromCallable(()->{
            sendMessage("data-out-0", new Event(CREATE, body.getChartId(), body));

            return body;
        }).subscribeOn(publishEventScheduler);
    }

    @Override
    public Mono<Void> populateChartFromMother(Data body) {
        return Mono.fromRunnable(()-> sendMessage("data-out-0", new Event(CREATE, body.getChartId(), body)))
                .subscribeOn(publishEventScheduler).then();
    }

    @Override
    public Flux<Data> getData(String studentId) {
        URI url = UriComponentsBuilder.fromUriString(DATA_SERVICE_URL + "?studentId={studentId}").build(studentId);
        LOG.debug("Will call the getData API on URL: {}", url);

        return webClient.get().uri(url)
                .retrieve().bodyToFlux(Data.class)
                .onErrorResume(error -> empty()).log(LOG.getName(), FINE);
    }

    @Override
    public Flux<Data> getChartData(String chartId) {
        URI url = UriComponentsBuilder.fromUriString(CHART_DATA_SERVICE_URL + "?chartId={chartId}").build(chartId);
        LOG.debug("Will call the getChartData API on URL: {}", url);

        return webClient.get().uri(url)
                .retrieve().bodyToFlux(Data.class)
                .onErrorResume(error -> empty()).log(LOG.getName(), FINE);
    }

    @Override
    public Flux<Data> getAllData() {
        URI url = UriComponentsBuilder.fromUriString(DATA_ALL).build().toUri();
        LOG.debug("Will get All Data");
        return webClient.get().uri(url)
                .retrieve().bodyToFlux(Data.class)
                .onErrorResume(error -> empty())
                .log(LOG.getName(), FINE);
    }

    @Override
    public Mono<List<String>> getSemesters(String studentId) {
        URI url = UriComponentsBuilder.fromUriString(GET_SEMESTER + "/{studentId}").build(studentId);
        return webClient.get().uri(url).retrieve().bodyToMono(new ParameterizedTypeReference<List<String>>() {})
                .onErrorResume(error->empty())
                .log(LOG.getName(), FINE);
    }

    @Override
    public Mono<Void> updateData(Data body) {
        return Mono.fromRunnable(() -> {
            sendMessage("data-out-0", new Event(UPDATE, body.getChartId(), body));
        }).subscribeOn(publishEventScheduler).then();
    }

    @Override
    public Mono<Void> deleteData(String chartId) {
        return Mono.fromRunnable(() -> sendMessage("data-out-0", new Event(DELETE, chartId, null)))
                .subscribeOn(publishEventScheduler).then();
    }

    private void sendMessage(String bindingName, Event event) {
        LOG.debug("Sending a {} message to {}", event.getEventType(), bindingName);
        Message message = MessageBuilder.withPayload(event)
                .setHeader("partitionKey", event.getKey())
                .build();
        streamBridge.send(bindingName, message);
    }

    private Throwable handleException(Throwable ex) {

        if (!(ex instanceof WebClientResponseException)) {
            LOG.warn("Got a unexpected error: {}, will rethrow it", ex.toString());
            return ex;
        }

        WebClientResponseException wcre = (WebClientResponseException)ex;

        switch (wcre.getStatusCode()) {

            case NOT_FOUND:
                return new NotFoundException(getErrorMessage(wcre));

            case UNPROCESSABLE_ENTITY :
                return new InvalidInputException(getErrorMessage(wcre));

            default:
                LOG.warn("Got an unexpected HTTP error: {}, will rethrow it", wcre.getStatusCode());
                LOG.warn("Error body: {}", wcre.getResponseBodyAsString());
                return ex;
        }
    }

    private String getErrorMessage(WebClientResponseException ex) {
        try {
            return mapper.readValue(ex.getResponseBodyAsString(), HttpErrorInfo.class).getMessage();
        } catch (IOException ioex) {
            return ex.getMessage();
        }
    }

}
