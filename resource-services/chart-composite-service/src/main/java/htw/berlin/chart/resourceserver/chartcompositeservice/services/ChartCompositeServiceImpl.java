package htw.berlin.chart.resourceserver.chartcompositeservice.services;


import static java.util.logging.Level.FINE;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import htw.berlin.recourceserver.util.http.ServiceUtil;
import htw.berlin.resourceserver.api.composite.chart.*;
import htw.berlin.resourceserver.api.core.chart.Chart;
import htw.berlin.resourceserver.api.core.data.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@RestController
public class ChartCompositeServiceImpl implements IChartCompositeService {

    private static final Logger LOG = LoggerFactory.getLogger(ChartCompositeServiceImpl.class);

    private final SecurityContext nullSecCtx = new SecurityContextImpl();

    private final ServiceUtil serviceUtil;

    private final ChartCompositeIntegration integration;

    @Autowired
    public ChartCompositeServiceImpl(ServiceUtil serviceUtil, ChartCompositeIntegration integration) {
        this.serviceUtil = serviceUtil;
        this.integration = integration;
    }


    @Override
    public Mono<Void> createChart(ChartSummary body) {
        try {
            List<Mono> monoList = new ArrayList<>();
            LOG.debug("createChart: creates a new chart entity for chartID: {}", body.getChartId());

            Chart chart = new Chart(body.getChartId(), body.getStudentId(), body.getChartType(), body.getChartLabel(),
                    new ArrayList<>(), body.getMotherChartId(),null);

            if (body.getMotherChartId() != null){
                //Put data in the child chart from the mother chart
                monoList.add(integration.populateChartFromMother(new Data(body.getChartId(), body.getMotherChartId(),
                        null, body.getStudentId(), null, new ArrayList<>(),null)));
            }

            monoList.add(integration.createChart(chart));

            return Mono.zip(r -> "", monoList.toArray(new Mono[0]))
                    .doOnError(ex -> LOG.warn("createChart failed: {}", ex.toString()))
                    .then();
        }catch (RuntimeException re){
            LOG.warn("createChart failed: {}", re.toString());
            throw  re;
        }
    }

    @Override
    public Mono<Void> createData(DataSummary body) {
        try {
            List<Mono> monoList = new ArrayList<>();
            LOG.debug("createData: creates a new data entity for chartID: {}", body.getChartId());

            Data data = new Data(body.getChartId(), body.getMotherChartId(),body.getDataId(), body.getStudentId(), body.getTranscript(), new ArrayList<>(),null);
            monoList.add(integration.createData(data));


            return Mono.zip(r -> "", monoList.toArray(new Mono[0]))
                    .doOnError(ex -> LOG.warn("createData failed: {}", ex.toString()))
                    .then();
        }catch (RuntimeException re){
            LOG.warn("createData failed: {}", re.toString());
            throw  re;
        }
    }

    @Override
    public Mono<GetChartAggregate> getCharts(String studentId) {
        LOG.info("Will get composite chart info for chart.id={}", studentId);

        return Mono.zip(
                values -> createChartAggregates(
                (SecurityContext) values[0], (List<Chart>)values[1], (List<Data>)values[2], serviceUtil.getServiceAddress()),
                getSecurityContextMono(),
                integration.getCharts(studentId).collectList(),
                integration.getData(studentId).collectList())
                .doOnError(ex -> LOG.warn("getCompositeChart failed: {}", ex.toString()))
                .log(LOG.getName(), FINE);
    }

    @Override
    public Mono<ChartAggregate> getChartsData(String chartId) {
        return Mono.zip(
                values -> createDataSummery((List<Data>)values[0], (Chart)values[1]),
                integration.getChartData(chartId).collectList(),
                        integration.getChartChart(chartId))
                .doOnError(ex -> LOG.warn("getChartsData failed: {}", ex.toString()))
                .log(LOG.getName(), FINE);
    }

    @Override
    public Mono<GlobalChartDataWrapper> getGlobalChart(String studentId) {
        return Mono.zip(
                values -> chartGlobalChart((List<Data>) values[0], (List<Data>) values[1]),
                integration.getAllData().collectList(),
                        integration.getData(studentId).collectList())
                .doOnNext(ex -> LOG.warn("getGlobalChart failed: {}", ex.toString()))
                .log(LOG.getName(), FINE);
    }

    @Override
    public Mono<List<String>> getSemesters(String studentId) {
        return integration.getSemesters(studentId);
    }

    private GlobalChartDataWrapper chartGlobalChart(List<Data> data, List<Data> studentData) {
        GlobalChartDataWrapper globalChartDataWrapper = new GlobalChartDataWrapper(prepareGlobalChart(data), prepareGlobalChart(studentData));
        return globalChartDataWrapper;
    }

    private List<GlobalChartData> prepareGlobalChart(List<Data> data) {

        List<GlobalChartData> globalChartDataList = new ArrayList<GlobalChartData>();
        //Sort Data by SemesterYear
        Collections.sort(data);
        //Get Semesters-List
        List<String> semesters = data.stream().map(d->d.getTranscript().getSemesterLabel()).collect(Collectors.toList());
        semesters = semesters.stream().distinct().collect(Collectors.toList());

        semesters.forEach(s->{
            Double sumAverageSemester =
                    data.stream().filter(d-> d.getTranscript().getSemesterLabel()
                            .equals(s)).mapToDouble(data1 -> data1.getTranscript().getAverage()).sum();
            long count = data.stream().filter(d-> d.getTranscript().getSemesterLabel()
                    .equals(s)).count();
            Double totalAverageSemester =  count != 0 ?  sumAverageSemester / count : 0.0;
            globalChartDataList.add(new GlobalChartData(s, totalAverageSemester));
        });

        return globalChartDataList;
    }


    @Override
    public Mono<Void> updateChart(ChartAggregate body) {
        LOG.info("Will update composite chart info for chart.id={}", body.getChartSummary().getChartId());

        List<DataSummary> dataSummaryList = body.getDataSummaryList();
        try {
            List<Mono> monoList = new ArrayList<>();
            dataSummaryList.forEach(dataSummary -> {
                Data data = new Data(dataSummary.getChartId(),
                        dataSummary.getMotherChartId(),
                        dataSummary.getDataId(),
                        dataSummary.getStudentId(),
                        dataSummary.getTranscript(),
                        dataSummary.getChildDataIds(),
                        null);
                monoList.add(integration.updateData(data));
            });

            return Mono.zip(r -> "", monoList.toArray(new Mono[0]))
                    .doOnError(ex -> LOG.warn("updateData failed: {}", ex.toString()))
                    .then();
        }catch (RuntimeException re){
            LOG.warn("updateData failed: {}", re.toString());
            throw  re;
        }
    }

    @Override
    public Mono<Void> deleteChart(String chartId) {
        try {
            LOG.debug("deleteCompositeChart : Delete a chart aggregate for chartId: {}", chartId);

            return Mono.zip(r->"",
                    getLogAuthorizationInfoMono(),
                    integration.deleteChart(chartId),
                    integration.deleteData(chartId))
                    .doOnError(ex -> LOG.warn("delete failed: {}", ex.toString()))
                    .log(LOG.getName(), FINE).then();
        }catch (RuntimeException re){
            LOG.warn("deleteCompositeChart failed: {}", re.toString());
            throw re;
        }
    }


    private ChartAggregate createDataSummery(List<Data> data, Chart chart) {
        List<DataSummary> dataSummaryList =
                data
                        .stream()
                        .map(d -> new DataSummary(d.getDataId(),d.getChartId(), d.getMotherChartId(),d.getStudentId(), d.getTranscript(), d.getChildDataIds()))
                        .collect(Collectors.toList());
        ChartSummary chartSummary = new ChartSummary(chart.getChartId(), chart.getStudentId(), chart.getChartLabel(),chart.getChartType(), chart.getMotherChartId());
        return new ChartAggregate(chartSummary, dataSummaryList, null);
    }

    private GetChartAggregate createChartAggregates(SecurityContext sc, List<Chart> charts, List<Data> data, String serviceAddress) {

        logAuthorizationInfo(sc);

        List<ChartAggregate> chartAggregates = new ArrayList<>();


        chartAggregates = charts.stream().map(c -> {

            //map to ChartSummery
            ChartSummary chartSummary = new ChartSummary(c.getChartId(), c.getStudentId(), c.getChartLabel(), c.getChartType(), c.getMotherChartId());

            //Get all data associated to the Chart
            List<Data> dataList = (data == null) ? new ArrayList<>() : data.stream().filter(d -> d.getChartId().equals(c.getChartId())).collect(Collectors.toList());

            Collections.sort(dataList);

            //map to DataSummary
            List<DataSummary> dataSummaryList = (dataList == null) ? new ArrayList<>() : dataList.stream().map(d ->
                    new DataSummary(d.getDataId(), d.getChartId(), d.getMotherChartId(),d.getStudentId(), d.getTranscript(), d.getChildDataIds())).collect(Collectors.toList());

            //Get serviceAddresses
            ServiceAddresses serviceAddresses = new ServiceAddresses(serviceAddress,
                    c.getServiceAddress(),
                    data.stream()
                            .map(d -> d.getServiceAddress())
                            .collect(Collectors.toList())
            );

            return new ChartAggregate(chartSummary, dataSummaryList, serviceAddresses);

        }).collect(Collectors.toList());

        return new GetChartAggregate(chartAggregates);
    }


    private Mono<SecurityContext> getLogAuthorizationInfoMono() {
        return getSecurityContextMono().doOnNext(sc -> logAuthorizationInfo(sc));
    }

    private Mono<SecurityContext> getSecurityContextMono() {
        return ReactiveSecurityContextHolder.getContext().defaultIfEmpty(nullSecCtx);
    }

    private void logAuthorizationInfo(SecurityContext sc) {
        if (sc != null && sc.getAuthentication() != null && sc.getAuthentication() instanceof JwtAuthenticationToken) {
            Jwt jwtToken = ((JwtAuthenticationToken)sc.getAuthentication()).getToken();
            logAuthorizationInfo(jwtToken);
        } else {
            LOG.warn("No JWT based Authentication supplied, running tests are we?");
        }
    }

    private void logAuthorizationInfo(Jwt jwt) {
        if (jwt == null) {
            LOG.warn("No JWT supplied, running tests are we?");
        } else {
            if (LOG.isDebugEnabled()) {
                URL issuer = jwt.getIssuer();
                List<String> audience = jwt.getAudience();
                Object subject = jwt.getClaims().get("sub");
                Object scopes = jwt.getClaims().get("scope");
                Object expires = jwt.getClaims().get("exp");

                LOG.debug("Authorization info: Subject: {}, scopes: {}, expires {}: issuer: {}, audience: {}", subject, scopes, expires, issuer, audience);
            }
        }
    }
}
