package htw.berlin.chart.resourceserver.chartcompositeservice.config;

import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.server.SecurityWebFilterChain;

import static org.springframework.http.HttpMethod.*;

@EnableWebFluxSecurity
public class ChartCompositeConfig {
    @Bean
    SecurityWebFilterChain securityWebFilterChain(ServerHttpSecurity http) throws Exception {
        http
                .authorizeExchange()
                .pathMatchers("/openapi/**").permitAll()
                .pathMatchers("/webjars/**").permitAll()
                .pathMatchers("/actuator/**").permitAll()
                .pathMatchers("/login/**").permitAll()
                .pathMatchers(POST, "/chart-composite/**").hasAuthority("SCOPE_chart.write")
                .pathMatchers(PATCH, "/chart-composite/**").hasAuthority("SCOPE_chart.write")
                .pathMatchers(DELETE, "/chart-composite/**").hasAuthority("SCOPE_chart.write")
                .pathMatchers(GET, "/chart-composite/**").hasAuthority("SCOPE_chart.read")
                .pathMatchers(GET, "/chart-composite/chart-data/**").hasAuthority("SCOPE_chart.read")
                .anyExchange().authenticated()
                .and()
                .oauth2ResourceServer()
                .jwt();
        return http.build();
    }
}
