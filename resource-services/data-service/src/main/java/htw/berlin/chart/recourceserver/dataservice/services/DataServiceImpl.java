package htw.berlin.chart.recourceserver.dataservice.services;

import htw.berlin.chart.recourceserver.dataservice.persistence.DataEntity;
import htw.berlin.chart.recourceserver.dataservice.persistence.IDataRepository;
import htw.berlin.recourceserver.util.http.ServiceUtil;
import htw.berlin.resourceserver.api.core.data.Data;
import htw.berlin.resourceserver.api.core.data.IDataService;
import htw.berlin.resourceserver.api.exceptions.InvalidInputException;
import htw.berlin.resourceserver.api.exceptions.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static java.util.logging.Level.FINE;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;

@RestController
public class  DataServiceImpl implements IDataService {
    private static final Logger LOG = LoggerFactory.getLogger(DataServiceImpl.class);

    private final ServiceUtil serviceUtil;

    private final IDataRepository dataRepository;

    private final IDataMapper dataMapper;

    private final ReactiveMongoTemplate mongoTemplate;

    @Autowired
    public DataServiceImpl(ServiceUtil serviceUtil, IDataRepository dataRepository, IDataMapper dataMapper, ReactiveMongoTemplate mongoTemplate) {
        this.serviceUtil = serviceUtil;
        this.dataRepository = dataRepository;
        this.dataMapper = dataMapper;
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public Mono<Data> createData(Data body) {

        if (body.getChartId().isEmpty()) {
            throw new InvalidInputException("Invalid dataId: " + body.getDataId());
        }

        return Mono.fromCallable(()-> dataMapper.apiToEntity(body))
                        .log(LOG.getName(), FINE)
                        .map(value -> calculateSemesterAverageAndModulesCount(value))
                        .map(value -> calculateCreditsTotal(value))
                        .flatMap(value -> dataRepository.save(value))
                        .onErrorMap(
                        DuplicateKeyException.class,
                        ex -> new InvalidInputException("Duplicate key, Chart Id: " + body.getChartId() + ", Data Id: " + body.getDataId()))
                        .map(entity -> dataMapper.entityToApi(entity));
    }

    @Override
    public Mono<Void> populateChartFromMother(Data body) {
        LOG.info("Will populate child chart from mother chart with id={}", body.getMotherChartId());
        // Get Data
        return dataRepository.findAllByChartId(body.getMotherChartId())
                .doOnNext(motherD -> {
                    String childDataId = UUID.randomUUID().toString();
                    motherD.getChildDataIds().add(childDataId);
                    motherD.setChildDataIds(motherD.getChildDataIds());
                })
                .flatMap(motherD -> dataRepository.save(motherD))
                .map(motherD-> {
                    List<String> childIds = motherD.getChildDataIds().stream().collect(Collectors.toList());
                    String childId = childIds.get(childIds.size()-1);
                    return new DataEntity(childId, body.getChartId(), body.getMotherChartId(), body.getStudentId(), motherD.gettranscript(), null);
                })
                .flatMap(entity -> dataRepository.save(entity))
                .then();
    }

    @Override
    public Flux<Data> getData(String studentId) {
        if (studentId.isEmpty()) {
            throw new InvalidInputException("Invalid studentId: " + studentId);
        }
        LOG.info("Will get data info for chart with id={}", studentId);
        return dataRepository.findByStudentId(studentId)
                .log(LOG.getName(), FINE)
                .map(e -> dataMapper.entityToApi(e))
                .map(e -> setServiceAddress(e));
    }

    @Override
    public Flux<Data> getChartData(String chartId) {
        if (chartId.isEmpty()) {
            throw new InvalidInputException("Invalid chartId: " + chartId);
        }

        LOG.info("Will get data info for chart with id={}", chartId);
        return dataRepository.findAllByChartId(chartId)
                .log(LOG.getName(), FINE)
                .map(e -> dataMapper.entityToApi(e))
                .map(e -> setServiceAddress(e));
    }

    @Override
    public Flux<Data> getAllData() {
        return dataRepository.findAll()
                .map(e -> dataMapper.entityToApi(e));
    }

    public Flux<String> x(String studentId){
        return dataRepository.findByStudentId(studentId)
                .map(d->d.gettranscript().getSemesterLabel());
    }
    @Override
    public Mono<List<String>> getSemesters(String studentId) {
        return dataRepository.findByStudentId(studentId)
                .map(d->d.gettranscript().getSemesterLabel())
                .distinct()
                .collectList();
    }
    @Override
    public Mono<Void> updateData(Data body) {
        if (body.getChartId().isEmpty()) {
            throw new InvalidInputException("Invalid chartId: " + body.getChartId());
        }
        LOG.info("Will update data with chart id: {}", body.getChartId());

        DataEntity newEntity = dataMapper.apiToEntity(body);
//        updateChildData(newEntity);

        if (newEntity.gettranscript().getGrades().isEmpty()) {
            //first delete Children releated to Mother
//            for (String dataId : newEntity.getChildDataIds()){
//                dataRepository.findByDataId(dataId)
//                        .flatMap(found -> dataRepository.delete(found));
//            }
            //then delete Mother
            return dataRepository.findByDataId(body.getDataId())
                    .flatMap(found -> dataRepository.delete(found));
        } else {
            return dataRepository.findByDataId(body.getDataId())
                    .doOnNext(foundEntity -> {
                        foundEntity.settranscript(newEntity.gettranscript());
                        calculateCreditsTotal(calculateSemesterAverageAndModulesCount(foundEntity));
                    })
                    .flatMap(foundEntity -> dataRepository.save(foundEntity))
                    .log(LOG.getName(), FINE)
                    .switchIfEmpty(Mono.error(new NotFoundException("updateDate: Can't find Data with data Id: " + body.getDataId())))
                    .then();
        }
    }
//    private void updateChildData(DataEntity entity) {
//        calculateCreditsTotal(calculateSemesterAverageAndModulesCount(entity));
//        DataEntity dataEntity = dataRepository.findByDataId(entity.getDataId()).block();
//        for (String childId: dataEntity.getChildDataIds()) {
//            DataEntity childData = dataRepository.findByDataId(childId).block();
//            childData.settranscript(entity.gettranscript());
//            dataRepository.save(childData).block();
//        }
//    }

    private DataEntity calculateSemesterAverageAndModulesCount(DataEntity entity) {
        if (entity.gettranscript() != null){
            double sum = entity.gettranscript().getGrades().stream().mapToDouble(value -> value.getGrade()).sum();
            /* count and set modules for transcript */
            entity.gettranscript().setModulesCount(entity.gettranscript().getGrades().stream().count());
            entity.gettranscript().setAverage(entity.gettranscript().getModulesCount() != 0 ? (sum/entity.gettranscript().getModulesCount()) : 0.0);
        }
        return entity;
    }

    private DataEntity calculateCreditsTotal(DataEntity entity) {
        if (entity.gettranscript() != null) {
            entity.gettranscript().setCreditsTotal(entity.gettranscript().getGrades().stream().mapToInt(value -> value.getCredits()).sum());
        }
        return entity;
    }

    @Override
    public Mono<Void> deleteData(String chartId) {
        if (chartId.isEmpty()) {
            throw new InvalidInputException("Invalid chartId: " + chartId);
        }


        LOG.debug("deleteData: tries to delete data for the chart with chartId: {}", chartId);

        return dataRepository.deleteAllByChartId(chartId);
    }

    private Data setServiceAddress(Data e) {
        e.setServiceAddress(serviceUtil.getServiceAddress());
        return e;
    }

}
