package htw.berlin.chart.recourceserver.dataservice.services;

import htw.berlin.chart.recourceserver.dataservice.persistence.DataEntity;
import htw.berlin.chart.recourceserver.dataservice.persistence.ModuleGradeObj;
import htw.berlin.chart.recourceserver.dataservice.persistence.TranscriptObj;
import htw.berlin.resourceserver.api.core.data.Data;
import htw.berlin.resourceserver.api.core.data.ModuleGrade;
import htw.berlin.resourceserver.api.core.data.Transcript;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface IDataMapper {

    @Mappings({
            @Mapping(target = "serviceAddress", ignore = true)
    })
    Data entityToApi(DataEntity entity);

    @Mappings({
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "version", ignore = true)
    })
    DataEntity apiToEntity(Data api);


    Transcript transcriptObjToTranscript(TranscriptObj transcriptObj);
    TranscriptObj transcriptToTranscriptObj(Transcript transcript);

    ModuleGrade moduleGradeObjToModuleGrade(ModuleGradeObj moduleGradeObj);
    ModuleGradeObj moduleGradeToModuleGradeObj(ModuleGrade moduleGrade);

}
