package htw.berlin.chart.recourceserver.dataservice.persistence;

public class SemesterAverage {
    private String semester;
    private Double totalAverage;

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public Double getTotalAverage() {
        return totalAverage;
    }

    public void setTotalAverage(Double totalAverage) {
        this.totalAverage = totalAverage;
    }
}
