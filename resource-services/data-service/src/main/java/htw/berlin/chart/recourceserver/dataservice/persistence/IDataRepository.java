package htw.berlin.chart.recourceserver.dataservice.persistence;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface IDataRepository extends  ReactiveCrudRepository<DataEntity, String> {

    Mono<DataEntity> findByChartId (String chartId);
    Mono<DataEntity> findByDataId (String dataId);
    Mono<DataEntity> deleteByChartId(String chartId);
    Flux<DataEntity> findAllByChartId(String chartId);
    Flux<DataEntity> findByStudentId(String studentId);
    Mono<Void> deleteAllByChartId(String chartId);

}
