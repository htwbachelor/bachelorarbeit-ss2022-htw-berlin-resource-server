package htw.berlin.chart.recourceserver.dataservice.persistence;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Collection;
import java.util.List;

@Document(collection = "data")
//@CompoundIndex(name = "chart-data-id", unique = true, def = "{'chartId': 1, 'dataId': 1}")
public class DataEntity {
    @Id
    private String id;
    @Version
    private Integer version;
    private String chartId;
    private String motherChartId;
    private String dataId;
    private String studentId;
    private TranscriptObj transcript;
    private Collection<String> childDataIds;


    public DataEntity() {
    }

    public DataEntity(String dataId, String chartId, String motherChartId, String studentId, TranscriptObj transcript, Collection<String> childDataIds) {
        this.dataId = dataId;
        this.chartId = chartId;
        this.motherChartId = motherChartId;
        this.studentId = studentId;
        this.transcript = transcript;
        this.childDataIds = childDataIds;
//        this.createdAt = createdAt;
//        this.lastUpdate = lastUpdate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getChartId() {
        return chartId;
    }

    public void setChartId(String chartId) {
        this.chartId = chartId;
    }

    public String getMotherChartId() {
        return motherChartId;
    }

    public void setMotherChartId(String motherChartId) {
        this.motherChartId = motherChartId;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getDataId() {
        return dataId;
    }

    public void setDataId(String dataId) {
        this.dataId = dataId;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public TranscriptObj gettranscript() {
        return transcript;
    }

    public void settranscript(TranscriptObj transcript) {
        this.transcript = transcript;
    }

    public Collection<String> getChildDataIds() {
        return childDataIds;
    }

    public void setChildDataIds(Collection<String> childDataIds) {
        this.childDataIds = childDataIds;
    }


    @Override
    public boolean equals(Object obj) {

        if (obj == null){
            return false;
        }
        if (obj.getClass() != obj.getClass()){
            return false;
        }

        final DataEntity other = (DataEntity) obj;

        return this.getDataId() == ((DataEntity) obj).getDataId() &&
                this.getStudentId() == ((DataEntity) obj).getStudentId() &&
                this.gettranscript() == ((DataEntity) obj).gettranscript();
//                this.getCreatedAt() == ((DataEntity) obj).getCreatedAt();
    }
}
