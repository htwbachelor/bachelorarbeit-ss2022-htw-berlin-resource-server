package htw.berlin.chart.recourceserver.dataservice.persistence;

import org.springframework.data.mongodb.core.index.Indexed;

import java.util.Collection;

public class TranscriptObj {

    private String semester;

    private int semesterYear;
    private String semesterLabel;
    private Collection<ModuleGradeObj> grades;
    private long modulesCount;
    private int creditsTotal;
    private double average;



    public TranscriptObj() {
        semester = null;
        semesterYear = 0;
        semesterLabel = null;
        grades = null;
        modulesCount = 0;
        creditsTotal = 0;
        average = 0;
    }

    public TranscriptObj(String semester, int semesterYear,Collection<ModuleGradeObj> grades, long modulesCount,int creditsTotal, double average) {
        this.semester = semester;
        this.semesterYear = semesterYear;
        this.semesterLabel = semester + semesterYear;
        this.grades = grades;
        this.modulesCount = modulesCount;
        this.creditsTotal = creditsTotal;
        this.average = average;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public int getSemesterYear() {
        return semesterYear;
    }

    public void setSemesterYear(int semesterYear) {
        this.semesterYear = semesterYear;
    }

    public String getSemesterLabel() {
        return semesterLabel;
    }

    public void setSemesterLabel(String semesterLabel) {
        this.semesterLabel = semesterLabel;
    }

    public Collection<ModuleGradeObj> getGrades() {
        return grades;
    }

    public void setGrades(Collection<ModuleGradeObj> grades) {
        this.grades = grades;
    }

    public long getModulesCount() {
        return modulesCount;
    }

    public void setModulesCount(long modulesCount) {
        this.modulesCount = modulesCount;
    }

    public int getCreditsTotal() {
        return creditsTotal;
    }

    public void setCreditsTotal(int creditsTotal) {
        this.creditsTotal = creditsTotal;
    }

    public double getAverage() {
        return average;
    }

    public void setAverage(double average) {
        this.average = average;
    }
}
