package htw.berlin.chart.resourceserver.chartservice;

import com.mongodb.client.result.InsertManyResult;
import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoClients;
import com.mongodb.reactivestreams.client.MongoCollection;
import com.mongodb.reactivestreams.client.MongoDatabase;
import htw.berlin.chart.resourceserver.chartservice.persistence.ChartEntity;
import htw.berlin.chart.resourceserver.chartservice.persistence.ChartLabelObj;
import htw.berlin.chart.resourceserver.chartservice.persistence.ChartTypeObj;
import htw.berlin.resourceserver.api.core.chart.Chart;
import htw.berlin.resourceserver.api.core.chart.ChartLabel;
import htw.berlin.resourceserver.api.core.chart.ChartType;
import org.bson.Document;
import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.data.mapping.context.MappingContext;
import org.springframework.data.mongodb.core.ReactiveMongoOperations;
import org.springframework.data.mongodb.core.index.IndexResolver;
import org.springframework.data.mongodb.core.index.MongoPersistentEntityIndexResolver;
import org.springframework.data.mongodb.core.index.ReactiveIndexOperations;
import org.springframework.data.mongodb.core.mapping.MongoPersistentEntity;
import org.springframework.data.mongodb.core.mapping.MongoPersistentProperty;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@SpringBootApplication
@ComponentScan("htw.berlin")
public class ChartServiceApplication {

    private static final Logger LOG = LoggerFactory.getLogger(ChartServiceApplication.class);

    public static void main(String[] args) {
        ConfigurableApplicationContext ctx = SpringApplication.run(ChartServiceApplication.class, args);

        String mongodDbHost = ctx.getEnvironment().getProperty("spring.data.mongodb.host");
        String mongodDbPort = ctx.getEnvironment().getProperty("spring.data.mongodb.port");
        LOG.info("Connected to MongoDb: " + mongodDbHost + ":" + mongodDbPort);

        String uri = "mongodb://localhost:27017/chart-db";
        try (MongoClient mongoClient = MongoClients.create(uri)){
            MongoDatabase db = mongoClient.getDatabase("chart-db");
            MongoCollection<Document> coll = db.getCollection("charts");
        }

    }

    @Autowired
    ReactiveMongoOperations mongoTemplate;

    @EventListener(ContextRefreshedEvent.class)
    public void initIndicesAfterStartup() {

        MappingContext<? extends MongoPersistentEntity<?>, MongoPersistentProperty> mappingContext = mongoTemplate.getConverter().getMappingContext();
        IndexResolver resolver = new MongoPersistentEntityIndexResolver(mappingContext);

        ReactiveIndexOperations indexOps = mongoTemplate.indexOps(ChartEntity.class);
        resolver.resolveIndexFor(ChartEntity.class).forEach(e -> indexOps.ensureIndex(e).block());
    }
}
