package htw.berlin.chart.resourceserver.chartservice.persistence;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


public interface IChartRepository extends ReactiveCrudRepository<ChartEntity, String> {
    Mono<ChartEntity> findByChartId(String chartId);
    Flux<ChartEntity> findByStudentId(String studentId);
}
