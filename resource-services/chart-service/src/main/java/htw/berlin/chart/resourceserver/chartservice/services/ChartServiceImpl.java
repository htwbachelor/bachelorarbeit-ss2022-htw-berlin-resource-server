package htw.berlin.chart.resourceserver.chartservice.services;


import htw.berlin.chart.resourceserver.chartservice.persistence.ChartEntity;
import htw.berlin.chart.resourceserver.chartservice.persistence.ChartLabelObj;
import htw.berlin.chart.resourceserver.chartservice.persistence.ChartTypeObj;
import htw.berlin.chart.resourceserver.chartservice.persistence.IChartRepository;
import htw.berlin.recourceserver.util.http.ServiceUtil;
import htw.berlin.resourceserver.api.core.chart.Chart;
import htw.berlin.resourceserver.api.core.chart.ChartLabel;
import htw.berlin.resourceserver.api.core.chart.ChartType;
import htw.berlin.resourceserver.api.core.chart.IChartService;
import htw.berlin.resourceserver.api.exceptions.InvalidInputException;
import htw.berlin.resourceserver.api.exceptions.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static java.util.logging.Level.FINE;

@RestController
public class ChartServiceImpl implements IChartService {
    private static final Logger LOG = LoggerFactory.getLogger(ChartServiceImpl.class);

    private final ServiceUtil serviceUtil;

    private final IChartRepository repository;

    private final IChartMapper mapper;

    @Autowired
    public ChartServiceImpl(ServiceUtil serviceUtil, IChartRepository repository, IChartMapper mapper) {
        this.serviceUtil = serviceUtil;
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public Mono<Chart> createChart(Chart body) {
        if (body.getChartId().isEmpty()){
            throw new InvalidInputException("Invalid chartId: " + body.getChartId());
        }
        ChartEntity entity = mapper.apiToEntity(body);
        if (body.getMotherChartId() != null){
             updateChildChartIdsInMother(body);
        }

        Mono<Chart> newEntity = repository.save(entity)
                .log(LOG.getName(), FINE)
                .onErrorMap(
                        DuplicateKeyException.class,
                        ex -> new InvalidInputException("Duplicate key, Chart Id: " + body.getChartId()))
                .map(e -> mapper.entityToApi(e));

        return newEntity;
    }

    @Override
    public Flux<Chart> getCharts(String studentId) {
        if (studentId.isEmpty()){
            throw new InvalidInputException("Invalid studentId: " + studentId);
        }
        LOG.info("Will get chart info for id = {}", studentId);


        return repository.findByStudentId(studentId)
                .switchIfEmpty(Flux.error(new NotFoundException("No chart found for studentId: " + studentId)))
                .log(LOG.getName(), FINE)
                .map(e -> mapper.entityToApi(e))
                .map(e -> setServiceAddress(e));
    }

    @Override
    public Mono<Chart> getChartChart(String chartId) {
        if (chartId.isEmpty()){
            throw new InvalidInputException("Invalid chartId: " + chartId);
        }
        LOG.info("Will get chart info for id = {}", chartId);


        return repository.findByChartId(chartId)
                .switchIfEmpty(Mono.error(new NotFoundException("No chart found for chartId: " + chartId)))
                .log(LOG.getName(), FINE)
                .map(e -> mapper.entityToApi(e))
                .map(e -> setServiceAddress(e));
    }

    private void updateChildChartIdsInMother(Chart body) {
        ChartEntity chart = repository.findByChartId(body.getMotherChartId()).block();
        repository.save(updateChildren(chart, body)).block();
    }

    private ChartEntity updateChildren(ChartEntity chartEntity, Chart body) {
        LOG.info("update MotherChart for child with id = {}", body.getChartId());
        if (body.getChartId() != null){
            Collection<String> ids = chartEntity.getChildChartIds();
            ids.add(body.getChartId());
            chartEntity.setChildChartIds(ids);
        }
        return chartEntity;
    }



    @Override
    public Mono<Void> deleteChart(String chartId) {
        if (chartId.isEmpty()) {
            throw new InvalidInputException("Invalid chartId: " + chartId);
        }
        //First delete Child from mother
        repository.findByChartId(chartId)
                        .map(chartEntity -> repository.findByChartId(chartEntity.getMotherChartId()))
                        .map(motherChart -> deleteChildFromMother(motherChart, chartId));
        LOG.debug("deleteChart: tries to delete an entity with chartId: {}", chartId);
        // then delete Child Entity
        return repository.findByChartId(chartId).log(LOG.getName(), FINE).map(e -> repository.delete(e)).flatMap(e -> e);
    }

    private Mono<Object> deleteChildFromMother(Mono<ChartEntity> motherChart, String chartId) {
        return motherChart.map(mc -> mc.getChildChartIds().remove(chartId));
    }

    private Chart setServiceAddress(Chart e) {
        e.setServiceAddress(serviceUtil.getServiceAddress());
        return e;
    }
}
