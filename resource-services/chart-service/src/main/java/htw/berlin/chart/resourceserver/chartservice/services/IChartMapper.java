package htw.berlin.chart.resourceserver.chartservice.services;


import htw.berlin.chart.resourceserver.chartservice.persistence.ChartEntity;
import htw.berlin.resourceserver.api.core.chart.Chart;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface IChartMapper {
    @Mappings({
            @Mapping(target= "serviceAddress", ignore = true),
            @Mapping(target = "chartLabel", source = "chartLabelObj"),
            @Mapping(target = "chartType", source = "chartTypeObj")
    })
    Chart entityToApi(ChartEntity entity);

    @Mappings({
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "version", ignore = true),
            @Mapping(target = "chartLabelObj", source = "chartLabel"),
            @Mapping(target = "chartTypeObj", source = "chartType")
    })
    ChartEntity apiToEntity(Chart chart);
}
