package htw.berlin.chart.resourceserver.chartservice.persistence;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Collection;
import java.util.Date;
import java.util.List;


@Document(collection = "charts")
public class ChartEntity {

    @Id
    private String id;

    @Version
    private Integer version;

    @Indexed(unique = true)
    private String chartId;

    private String studentId;

    private ChartTypeObj chartTypeObj;

    private ChartLabelObj chartLabelObj;

    private Collection<String> childChartIds;
    private String motherChartId;

    public ChartEntity() {
    }

    public ChartEntity(String chartId, String studentId, ChartTypeObj chartTypeObj, ChartLabelObj chartLabelObj, Collection<String> childChartIds, String motherChartId) {
        this.chartId = chartId;
        this.studentId = studentId;
        this.chartTypeObj = chartTypeObj;
        this.chartLabelObj = chartLabelObj;
        this.childChartIds = childChartIds;
        this.motherChartId = motherChartId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getChartId() {
        return chartId;
    }

    public void setChartId(String chartId) {
        this.chartId = chartId;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public ChartTypeObj getChartTypeObj() {
        return chartTypeObj;
    }

    public void setChartTypeObj(ChartTypeObj chartTypeObj) {
        this.chartTypeObj = chartTypeObj;
    }

    public ChartLabelObj getChartLabelObj() {
        return chartLabelObj;
    }

    public void setChartLabelObj(ChartLabelObj chartLabelObj) {
        this.chartLabelObj = chartLabelObj;
    }

    public Collection<String> getChildChartIds() {
        return childChartIds;
    }

    public void setChildChartIds(Collection<String> childChartIds) {
        this.childChartIds = childChartIds;
    }

    public String getMotherChartId() {
        return motherChartId;
    }

    public void setMotherChartId(String motherChartId) {
        this.motherChartId = motherChartId;
    }
}
