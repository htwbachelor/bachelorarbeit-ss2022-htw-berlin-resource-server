package htw.berlin.resourceserver.api.core.chart;

import htw.berlin.resourceserver.api.core.data.Data;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


public interface IChartService {
    Mono<Chart> createChart(Chart body);

    @GetMapping(
            value = "/chart",
            produces = "application/json"
    )
    Flux<Chart> getCharts(@RequestParam(value = "studentId", required = true) String studentId) ;

    @GetMapping(
            value = "/chart-chart",
            produces = "application/json"
    )
    Mono<Chart> getChartChart(@RequestParam(value = "chartId", required = true) String chartId);

//    Mono<Chart> updateChart(Chart body);

    Mono<Void> deleteChart(String chartId);
}
