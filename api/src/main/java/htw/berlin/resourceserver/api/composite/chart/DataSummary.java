package htw.berlin.resourceserver.api.composite.chart;


import htw.berlin.resourceserver.api.core.data.Transcript;

import java.util.ArrayList;
import java.util.Collection;

public class DataSummary {

    private final String dataId;

    private final String chartId;

    private final String motherChartId;

    private final String studentId;


    private final Transcript transcript;

    private Collection<String> childDataIds;

    public DataSummary() {
        dataId = null;
        chartId = null;
        motherChartId = null;
        studentId = null;
        transcript = null;
        childDataIds = new ArrayList<>();
    }
    public DataSummary(String dataId, String chartId, String motherChartId,String studentId, Transcript transcript, Collection<String> childDataIds) {
        this.dataId = dataId;
        this.chartId = chartId;
        this.motherChartId = motherChartId;
        this.studentId = studentId;
        this.transcript = transcript;
        this.childDataIds = childDataIds;

    }

    public String getDataId() {
        return dataId;
    }

    public String getChartId() {
        return chartId;
    }

    public String getMotherChartId() {
        return motherChartId;
    }

    public String getStudentId() {
        return studentId;
    }
    public Transcript getTranscript() {
        return transcript;
    }

    public Collection<String> getChildDataIds() {
        return childDataIds;
    }

    public void setChildDataIds(Collection<String> childDataIds) {
        this.childDataIds = childDataIds;
    }
}
