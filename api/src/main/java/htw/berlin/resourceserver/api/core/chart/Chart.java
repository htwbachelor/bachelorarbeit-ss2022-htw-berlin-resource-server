package htw.berlin.resourceserver.api.core.chart;


import java.util.ArrayList;
import java.util.List;

public class Chart {
    private String chartId;
    private String studentId;
    private ChartLabel chartLabel;
    private ChartType chartType;
    private List<String> childChartIds;
    private String motherChartId;
    private String serviceAddress;


    public Chart() {
        chartId = null;
        studentId = null;
        chartLabel = null;
        chartType = null;
        childChartIds = new ArrayList<>();
        motherChartId = null;
        serviceAddress = null;
    }

    public Chart(String chartId, String studentId, ChartType chartType, ChartLabel chartLabel, List<String> childChartIds, String motherChartId, String serviceAddress) {
        this.chartId = chartId;
        this.studentId = studentId;
        this.chartLabel = chartLabel;
        this.chartType = chartType;
        this.childChartIds = childChartIds;
        this.motherChartId = motherChartId;
        this.serviceAddress = serviceAddress;
    }

    public String getChartId() {
        return chartId;
    }

    public void setChartId(String chartId) {
        this.chartId = chartId;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public ChartLabel getChartLabel() {
        return chartLabel;
    }

    public void setChartLabel(ChartLabel chartLabel) {
        this.chartLabel = chartLabel;
    }

    public ChartType getChartType() {
        return chartType;
    }

    public void setChartType(ChartType chartType) {
        this.chartType = chartType;
    }

    public List<String> getChildChartIds() {
        return childChartIds;
    }

    public void setChildChartIds(List<String> childChartIds) {
        this.childChartIds = childChartIds;
    }

    public String getMotherChartId() {
        return motherChartId;
    }

    public void setMotherChartId(String motherChartId) {
        this.motherChartId = motherChartId;
    }

    public String getServiceAddress() {
        return serviceAddress;
    }

    public void setServiceAddress(String serviceAddress) {
        this.serviceAddress = serviceAddress;
    }
}
