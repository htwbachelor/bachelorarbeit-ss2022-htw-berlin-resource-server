package htw.berlin.resourceserver.api.composite.chart;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;


public interface IChartCompositeService {
    @PostMapping(
            value = "/chart-composite/createChart",
            consumes = "application/json"
    )
    Mono<Void> createChart(@RequestBody ChartSummary body);

    @PostMapping(
            value = "/chart-composite/createData",
            consumes = "application/json"
    )
    Mono<Void> createData(@RequestBody DataSummary body);

    @GetMapping(
            value = "/chart-composite/{studentId}",
            produces = "application/json")
    Mono<GetChartAggregate> getCharts(@PathVariable String studentId);

    @GetMapping(
            value = "/chart-composite/chart-data/{chartId}",
            produces = "application/json")
    Mono<ChartAggregate> getChartsData(@PathVariable String chartId);

    @GetMapping(
            value = "/chart-composite/global-chart/{studentId}",
            produces = "application/json"
    )
    Mono<GlobalChartDataWrapper> getGlobalChart(@PathVariable String studentId);

    @GetMapping(
            value = "/chart-composite/getSemester/{studentId}",
            produces = "application/json")
    Mono<List<String>> getSemesters(@PathVariable String studentId);

    @PatchMapping(
            value = "/chart-composite",
            consumes = "application/json")
    Mono<Void> updateChart(@RequestBody ChartAggregate body);

    @ResponseStatus(HttpStatus.ACCEPTED)
    @DeleteMapping(value = "/chart-composite/{chartId}")
    Mono<Void> deleteChart(@PathVariable String chartId);


}
