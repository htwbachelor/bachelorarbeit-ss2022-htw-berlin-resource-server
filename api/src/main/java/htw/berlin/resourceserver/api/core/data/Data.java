package htw.berlin.resourceserver.api.core.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Data implements Comparable<Data>{
    private String chartId;
    private String motherChartId;
    private String dataId;
    private String studentId;
    private Transcript transcript;
    private Collection<String> childDataIds;
    private String serviceAddress;



    public Data() {
        chartId = null;
        motherChartId = null;
        dataId = null;
        studentId = null;
        transcript = null;
        childDataIds = new ArrayList<>();
        serviceAddress = null;
    }

    public Data(String chartId, String motherChartId, String dataId, String studentId, Transcript transcript, Collection<String> childDataIds, String serviceAddress) {
        this.chartId = chartId;
        this.motherChartId = motherChartId;
        this.dataId = dataId;
        this.studentId = studentId;
        this.transcript = transcript;
        this.childDataIds = childDataIds;
        this.serviceAddress = serviceAddress;
    }

    public String getChartId() {
        return chartId;
    }

    public void setChartId(String chartId) {
        this.chartId = chartId;
    }

    public String getMotherChartId() {
        return motherChartId;
    }

    public void setMotherChartId(String motherChartId) {
        this.motherChartId = motherChartId;
    }

    public String getDataId() {
        return dataId;
    }

    public void setDataId(String dataId) {
        this.dataId = dataId;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public Transcript getTranscript() {
        return transcript;
    }

    public void setTranscript(Transcript transcript) {
        this.transcript = transcript;
    }

    public Collection<String> getChildDataIds() {
        return childDataIds;
    }

    public void setChildDataIds(Collection<String> childDataIds) {
        this.childDataIds = childDataIds;
    }

    public String getServiceAddress() {
        return serviceAddress;
    }

    public void setServiceAddress(String serviceAddress) {
        this.serviceAddress = serviceAddress;
    }


    @Override
    public int compareTo(Data data) {
        if (transcript.getSemesterYear() == 0 || data.getTranscript().getSemesterYear() == 0 ){
            return 0;
        }
        return transcript.getSemesterYear().compareTo(data.getTranscript().getSemesterYear());
    }
}
