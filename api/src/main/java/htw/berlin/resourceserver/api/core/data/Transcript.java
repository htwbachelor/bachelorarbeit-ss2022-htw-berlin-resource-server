package htw.berlin.resourceserver.api.core.data;

import java.util.Collection;

public class Transcript {
    private String semester;
    private Integer semesterYear;

    private String semesterLabel;
    private Collection<ModuleGrade> grades;
    private long modulesCount;
    private int creditsTotal;
    private double average;


    public Transcript() {
        semester = null;
        semesterYear = 0;
        semesterLabel = null;
        grades = null;
        modulesCount = 0;
        creditsTotal = 0;
        average = 0.0;
    }

    public Transcript(String semester, int semesterYear, Collection<ModuleGrade> grades, long modulesCount ,int creditsTotal, double average) {
        this.semester = semester;
        this.semesterYear = semesterYear;
        this.semesterLabel = semester + semesterYear;
        this.grades = grades;
        this.modulesCount = modulesCount;
        this.creditsTotal = creditsTotal;
        this.average = average;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public Integer getSemesterYear() {
        return semesterYear;
    }

    public void setSemesterYear(Integer semesterYear) {
        this.semesterYear = semesterYear;
    }

    public String getSemesterLabel() {
        return semesterLabel;
    }

    public void setSemesterLabel(String semesterLabel) {
        this.semesterLabel = semesterLabel;
    }

    public Collection<ModuleGrade> getGrades() {
        return grades;
    }

    public void setGrades(Collection<ModuleGrade> grades) {
        this.grades = grades;
    }

    public long getModulesCount() {
        return modulesCount;
    }

    public void setModulesCount(long modulesCount) {
        this.modulesCount = modulesCount;
    }

    public int getCreditsTotal() {
        return creditsTotal;
    }

    public void setCreditsTotal(int creditsTotal) {
        this.creditsTotal = creditsTotal;
    }

    public double getAverage() {
        return average;
    }

    public void setAverage(double average) {
        this.average = average;
    }
}
