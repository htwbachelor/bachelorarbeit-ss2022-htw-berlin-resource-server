package htw.berlin.resourceserver.api.composite.chart;

import javax.swing.event.ListDataEvent;
import java.util.ArrayList;
import java.util.List;

public class GlobalChartDataWrapper {
    private final List<GlobalChartData> globalChartDataList;
    private final List<GlobalChartData> studentDataList;


    public GlobalChartDataWrapper() {
        globalChartDataList = new ArrayList<>();
        studentDataList = new ArrayList<>();
    }

    public GlobalChartDataWrapper(List<GlobalChartData> globalChartDataList, List<GlobalChartData> studentDataList) {
        this.globalChartDataList = globalChartDataList;
        this.studentDataList = studentDataList;
    }


    public List<GlobalChartData> getGlobalChartDataList() {
        return globalChartDataList;
    }


    public List<GlobalChartData> getStudentDataList() {
        return studentDataList;
    }
}
