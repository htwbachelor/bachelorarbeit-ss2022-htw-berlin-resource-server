package htw.berlin.resourceserver.api.composite.chart;

import htw.berlin.resourceserver.api.core.chart.Chart;
import htw.berlin.resourceserver.api.core.chart.ChartLabel;
import htw.berlin.resourceserver.api.core.chart.ChartType;

import java.util.ArrayList;
import java.util.List;

public class ChartAggregate {

    private final ChartSummary chartSummary;
    private final List<DataSummary> dataSummaryList;
    private final ServiceAddresses serviceAddresses;


    public ChartAggregate() {
        chartSummary = null;
        dataSummaryList = new ArrayList<>();
        serviceAddresses = null;

    }

    public ChartAggregate(ChartSummary chartSummary, List<DataSummary> dataSummaryList, ServiceAddresses serviceAddresses) {

        this.chartSummary = chartSummary;
        this.dataSummaryList = dataSummaryList;
        this.serviceAddresses = serviceAddresses;
    }

    public ChartSummary getChartSummary() {
        return chartSummary;
    }

    public List<DataSummary> getDataSummaryList() {
        return dataSummaryList;
    }

    public ServiceAddresses getServiceAddresses() {
        return serviceAddresses;
    }
}
