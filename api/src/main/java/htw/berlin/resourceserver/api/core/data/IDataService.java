package htw.berlin.resourceserver.api.core.data;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

public interface IDataService {
    Mono<Data> createData(Data body);

    Mono<Void> populateChartFromMother(Data body);

    @GetMapping(
            value = "/data",
            produces = "application/json"
    )
    Flux<Data> getData(@RequestParam(value = "studentId", required = true) String studentId);

    @GetMapping(
            value = "/chart-data",
            produces = "application/json"
    )
    Flux<Data> getChartData(@RequestParam(value = "chartId", required = true) String chartId);

    @GetMapping(
            value = "/data-all",
            produces = "application/json"
    )
    Flux<Data> getAllData();

    @GetMapping(
            value = "/getSemesters/{studentId}",
            produces = "application/json"
    )
    Mono<List<String>> getSemesters(@PathVariable String studentId);

    Mono<Void> updateData(Data body);

    Mono<Void> deleteData(String chartId);
}
