package htw.berlin.resourceserver.api.composite.chart;

import htw.berlin.resourceserver.api.core.chart.ChartLabel;
import htw.berlin.resourceserver.api.core.chart.ChartType;

public class ChartSummary {
    private final String chartId;
    private final String studentId;
    private final ChartLabel chartLabel;
    private final ChartType chartType;
    private final String motherChartId;

    public ChartSummary() {
        chartId = null;
        studentId = null;
        chartLabel = null;
        chartType = null;
        motherChartId = null;
    }

    public ChartSummary(String chartId, String studentId, ChartLabel chartLabel, ChartType chartType, String motherChartId) {
        this.chartId = chartId;
        this.studentId = studentId;
        this.chartLabel = chartLabel;
        this.chartType = chartType;
        this.motherChartId = motherChartId;
    }

    public String getChartId() {
        return chartId;
    }

    public String getStudentId() {
        return studentId;
    }

    public ChartLabel getChartLabel() {
        return chartLabel;
    }

    public ChartType getChartType() {
        return chartType;
    }

    public String getMotherChartId() {
        return motherChartId;
    }
}
