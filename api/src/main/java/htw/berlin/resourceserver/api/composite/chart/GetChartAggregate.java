package htw.berlin.resourceserver.api.composite.chart;
;

import java.util.List;

public class GetChartAggregate {
    private final List<ChartAggregate> chartAggregates;
//    private final List<ChartSummary> chartSummary;
//    private final List<DataSummary> dataSummary;
//    private final ServiceAddresses serviceAddresses;


    public GetChartAggregate() {
        chartAggregates = null;
//        chartSummary = null;
//        dataSummary = null;
//        serviceAddresses = null;

    }

    public GetChartAggregate(List<ChartAggregate> chartAggregates) {
        this.chartAggregates = chartAggregates;
    }

    public List<ChartAggregate> getChartAggregates() {
        return chartAggregates;
    }

    //    public List<ChartSummary> getChartSummery() {
//        return chartSummary;
//    }
//    public List<DataSummary> getDataSummery() {
//        return dataSummary;
//    }
//
//    public ServiceAddresses getServiceAddresses() {
//        return serviceAddresses;
//    }
}
