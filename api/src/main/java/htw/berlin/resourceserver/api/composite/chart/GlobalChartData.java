package htw.berlin.resourceserver.api.composite.chart;

import htw.berlin.resourceserver.api.core.data.Data;

import java.util.List;

public class GlobalChartData {

    private String semester;
    private double average;

    public GlobalChartData() {
        semester = null;
        average = 0.0;
    }

    public GlobalChartData(String semester, double average) {
        this.semester = semester;
        this.average = average;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public double getAverage() {
        return average;
    }

    public void setAverage(double average) {
        this.average = average;
    }
}
